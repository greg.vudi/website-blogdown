+++
title = "About"
description = "About me"
date = "2022-10-05"
aliases = ["about-us", "about-hugo", "contact"]
author = "Hugo Authors"
enableComments = false
+++


**Greetings and salutations!**

My name is Gregor Vavdi and I'm the data wizard at the biggest retail bank in Slovenia (NLB). I've got some fancy degrees in applied mathematics (beachlor) and applied statistics (master) from the University of Ljubljana, but don't let that fool you - I'm still a total beginner when it comes to blogging.

**But why, you may ask?**

Well, I've had this idea for a blog simmering in my mind for what feels like centuries. I already have a blog about biking and other fun stuff, but let's just say my writing skills are a little bit...rusty.

So this blog is more like a public sandbox for me, a place where I can try out new ideas and document my experiments. Think of it as a GitHub repository with friendlier comments. It's just a tiny corner of the internet where I can share my thoughts and experiences with anyone who finds them interesting. And don't worry, I can't promise that everything here is 100% accurate, but I think it's still worth a read (or at least a good laugh).

**What's it all about?**

My new ideas and experiments will be based on the R programming language (for now), because we're BFFs and I use it all the time. I'll be writing about things like deploying R-based web applications and useful methodology, among other things. And every now and then, I'll try to share some thoughts on general ideas related to data, R, and current trends in these areas. Maybe one day I'll even invite some more qualified people to share their reliable thoughts with us all. Until then, you're stuck with me!


